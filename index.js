
/**
 * Module dependencies
 */
var Collection = require('deployd/lib/resources/collection'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    _ = require('underscore'),
    debug = require('debug')('dpd-push-notify'),
    _notify = require('push-notify'),
    GCM = _notify.gcm,
    APN = _notify.apn,
    WNS = _notify.wns;

/**
 * Module setup.
 */
function Pushnotifications(name, options) {
  Collection.apply(this, arguments);

  if(options && options.db) {
    this.notificationStore = process.server.createStore(this.name + "-notificationStore");
  }

  // see https://github.com/ToothlessGear/node-gcm
  // and https://github.com/argon/node-apn
  var config = this.config;

  if(!this.properties) {
    debug('No properties found, initializing with defaults');

    this.properties = {
      userId: {
        name: "userId",
        type: "string",
        typeLabel: "string",
        required: true,
        id: "userId",
        order: 0
      },
      token: {
        name: "token",
        type: "string",
        typeLabel: "string",
        required: true,
        id: "token",
        order: 1
      },
      deviceType: {
        name: "deviceType",
        type: "string",
        typeLabel: "string",
        required: true,
        id: "deviceType",
        order: 2
      }
    };

    // read and write file sync here, because else we can't make sure no other code would read the config before us
    var currentJSON = fs.readFileSync(path.join(options.configPath, 'config.json'), 'utf-8');
    var currentValue = JSON.parse(currentJSON);
    currentValue.properties = this.properties;
    fs.writeFileSync(path.join(options.configPath, 'config.json'), JSON.stringify(currentValue, null, '\t'));
  }

  this.gcm = GCM({
    apiKey: this.config.gcmApiServerKey
  });

  this.apn = APN({
    gateway: (this.config.apnGateway || "gateway.sandbox.push.apple.com"),
    cert: (this.config.apnCert || __dirname + "/../../config/cert.pem"),
    key: (this.config.apnKey || __dirname + "/../../config/key.pem")
  });
  this.apn.on('transmitted', function (result) {
    console.log('got transmitted result', result);
  });

  this.apn.on('transmissionError', function (error) {
    console.log('got error result', error);
  });

  this.wns = WNS({
    client_id: this.config.wnsClientId,
    client_secret: this.config.wnsClientSecret
  });
  this.wns.on('transmitted', function (result) {
    console.log('got transmitted result', result);
  });

  this.wns.on('transmissionError', function (error) {
    console.log('got error result', error);
  });
}

util.inherits(Pushnotifications, Collection);

Pushnotifications.dashboard = {
  path: path.join(__dirname, 'dashboard'),
  pages: ['Config', 'Devices', 'Notifications'],
  scripts: Collection.dashboard.scripts
};
// Pushnotifications.dashboard.pages = ["Config", "Data"]; //, "Events"];
// Pushnotifications.events = ['Register'];
Pushnotifications.label = "Push-Notify";
Pushnotifications.defaultPath = '/push';
Pushnotifications.events = [];
Pushnotifications.prototype.external = [];
Pushnotifications.prototype.clientGeneration = true;
Pushnotifications.prototype.clientGenerationGet = ['getNotifications'];
Pushnotifications.prototype.clientGenerationExec = ['register', 'send'];

Pushnotifications.basicDashboard = {
  settings: [
    {
      name        : 'gcmApiServerKey',
      type        : 'string',
      description : 'GCM API Server Key'
    },
    {
      name        : 'apnCert',
      type        : 'string',
      description : 'Location/contents of the cert.pem-File. Defaults to file named cert.pem in the config-directory of the app.'
    },
    {
      name        : 'apnKey',
      type        : 'string',
      description : 'Location/contents of the key.pem-File. Defaults to file named key.pem in the config-directory of the app.'
    },
    {
      name        : 'apnGateway',
      type        : 'string',
      description : 'APN Gateway. Defaults to sandbox'
    },
    {
      name        : 'wnsClientId',
      type        : 'string',
      description : 'WNS Client ID, which is not the Client ID you might be expecting, but rather the Package SID ("ms-app://...")'
    },
    {
      name        : 'wnsClientSecret',
      type        : 'string',
      description : 'WNS Client Secret'
    },
    {
      name        : 'defaultTitle',
      type        : 'string',
      description : 'Default title of the push notification. Defaults to \'Notification\''
    },
    {
      name        : 'defaultMessage',
      type        : 'string',
      description : 'Default message of the push notification. Defaults to \'Hi, something came up!\''
    },
    {
      name        : 'defaultTTL',
      type        : 'number',
      description : 'Default time to live of the push notification. Defaults to 3000 seconds'
    }
  ]
};

/**
 * Module methodes
 */
Pushnotifications.prototype.handle = function (ctx, next) {
  switch(ctx.req.method) {
    case 'POST':
      if(ctx.url === '/send') {
        return this.handleSend(ctx.body, ctx.done);
      } else if(ctx.url === '/register') {
        return this.handleRegister(ctx.body, ctx.req.session, ctx.done);
      }
      break;

    case 'GET':
      if(ctx.session.isRoot && ctx.url && ctx.url.indexOf('/getNotifications') === 0) {
        // id wasnt provided in the query
        if(ctx.url === '/getNotifications/count') {
          debug('returning notificationStore count');
          return this.handleGetNotificationsCount(ctx.query, ctx.done);
        } else {
          debug('returning notificationStore find');
          return this.handleGetNotifications(ctx.query, ctx.done);
        }
      }
      break;

    default:
      break;
  }

  // if we didn't catch this request, pass it on to the collection
  // but only do so, if the request came from an admin, i.e. from the dashboard
  if(ctx.session.isRoot) {
    return Collection.prototype.handle.apply(this, arguments);
  } else {
    return next();
  }
};

Pushnotifications.prototype.handleGetNotificationsCount = function(params, done) {
  return this.notificationStore.count(this.sanitizeQuery(params||{}), function (err, result) {
    if (err) return done(err);

    done(null, {count: result});
  });
};

Pushnotifications.prototype.handleGetNotifications = function(params, done) {
  return this.notificationStore.find(this.sanitizeQuery(params||{}), done);
};

Pushnotifications.prototype.handleRegister = function(params, session, done) {
  if(!session || !(session.isRoot || (session.user && session.user.id))) {
    return done('user is not logged in');
  }

  var self = this;
  if(!session.isRoot) {
    // the user calling "register" is always the user that is registering a device
    // except for root, that is
    params.userId = session.user.id;
  }

  if(!(params.userId && params.token && params.deviceType)) {
    return done('userId and token and deviceType are required');
  }

  this.store.find({
    token: params.token
  }, function(err, res) {
    if(err) {
      debug('Error when querying for existing tokens', err);
      return done(err);
    }

    if(res && res.length) {
      // TODO: handle new user id (i.e. existing device with new user)
      debug('device with this data already registered: %j', params);
      res = res[0];
      params.id = res.id;
      if(res.userId != params.userId || res.deviceType != params.deviceType) {
        // the userId/deviceType changed, so update the existing token
        debug('the user changed, so update the existing token: %j', res);
        res.userId = params.userId;
        self.store.update(res.id, params, function(err, cnt) {
          if(err) {
            return done(err);
          }
          done(null, params);
        });
        return;
      }
      return done(null, params);
    }

    debug('registering new device with this data: %j', params);

    // generate id here
    params.id = self.store.createUniqueIdentifier();
    self.store.insert(params, done);
  });
};

Pushnotifications.prototype.handleSend = function(body, done) {
  if(!body.query && body.message) {
    return done('query and message are required');
  }

  var self = this,
      query = {};
  ['userId', 'token', 'deviceType', 'id'].forEach(function(field) {
    if(body[field]) {
      query[field] = body[field];
      delete body[field];
    }
    if(body.query && body.query[field]) {
      query[field] = body.query[field];
      delete body.query[field];
    }
  });
  delete body.query;
  debug('querying for tokens: %j', query);
  this.store.find(query, function(err, results) {
    if(err || !results || !results.length) {
      return done('no token matched that query');
    }
    // generate id here
    var storeItem = {
      id: self.notificationStore.createUniqueIdentifier(),
      deviceIds: results,
      message: body
    };
    self.notificationStore.insert(storeItem, function(err, res) {
      if(err) {
        return done(err);
      }
      var notificationTokens = tokenItemsAsRegistrationIds(results);
      self._handleSend(body, notificationTokens, done);
    });
  });
};
Pushnotifications.prototype._handleSend = function(body, notificationTokens, done) {
  var sent = false;

  // apply defaults
  _.defaults(body.message, {
    timeToLive: this.config.defaultTTL || 3000
  });

  debug('handleSend: ', body, notificationTokens);

  // TODO: Build and test GCM
  if (false && notificationTokens && notificationTokens.gcmRegistrationIds && Array.isArray(notificationTokens.gcmRegistrationIds) && notificationTokens.gcmRegistrationIds.length > 0) {
    var gcmRegistrationIds = body.gcmRegistrationIds;
    var gcmNotification = this.parseBody('gcm', body);

    // the payload data to send...
    var message = new gcm.Message();
    if (body.title) {
      message.addData('title', body.title);
    } else {
      message.addData('title', this.config.defaultTitle || 'Notification' );
    }
    if (body.message) {
      message.addData('message', body.message);
    } else {
      message.addData('message',this.config.defaultMessage || 'Hi, something came up!' );
    }
    if (body.timeToLive) {
      message.timeToLive = body.timeToLive;
    } else {
      message.timeToLive = this.config.defaultTTL || 3000;
    }

    if (body.msgcnt) {
      message.addData('msgcnt', body.msgcnt);
    }
    if (body.soundname) {
      message.addData('soundname', body.soundname);
    }
    if (body.collapseKey) {
      message.collapseKey = body.collapseKey;
    }
    if (body.delayWhileIdle) {
      message.delayWhileIdle = body.delayWhileIdle;
    }

    /**
     * Parameters: message-literal, registrationIds-array, No. of retries, callback-function
     */
    // this.gcmSender.send(message, registrationIds, 4);
    sent = true;
  }

  if (notificationTokens && notificationTokens.apnTokens && Array.isArray(notificationTokens.apnTokens) && notificationTokens.apnTokens.length > 0) {
    var tokens = notificationTokens.apnTokens,
        apnNotification = this.parseBody('apn', body.message);

    apnNotification.token = tokens; // node-apn supports an array of tokens

    debug('apn: %j', apnNotification);
    this.apn.send(apnNotification);
    sent = true;
  }

  // TODO: Build and test WNS
  if (notificationTokens && notificationTokens.wnsRegistrationURIs && Array.isArray(notificationTokens.wnsRegistrationURIs) && notificationTokens.wnsRegistrationURIs.length > 0) {
    var wnsNotification = this.parseBody('wns', body.message);

    var index = 0;
    for (; index < notificationTokens.wnsRegistrationURIs.length; index++) {
      wnsNotification.channelURI = decodeURIComponent(notificationTokens.wnsRegistrationURIs[index]);
      debug('wns: %j', wnsNotification);
      this.wns.send(wnsNotification);
      // this.wns.send({
      //   channelURI: decodeURIComponent(body.wnsRegistrationURIs[index]),
      //   payload: body.payload,
      //   type: body.type,
      //   options: body.options
      // });
      sent = true;
    }
  }

  if(sent) {
    done(null, 'sent');
  } else {
    done('no notification was sent');
  }
};

function getToken(item) {
  return item.token;
}

function tokenItemsAsRegistrationIds(tokenItems) {
  return {
    apnTokens: tokenItems.filter(function(tokenItem) {
      return tokenItem.deviceType === 'ios';
    }).map(getToken),
    gcmRegistrationIds: tokenItems.filter(function(tokenItem) {
      return tokenItem.deviceType === 'android';
    }).map(getToken),
    wnsRegistrationURIs: tokenItems.filter(function(tokenItem) {
      return tokenItem.deviceType === 'wns';
    }).map(getToken)
  };
}

var fieldMappings = {
  $common: {
    badge: {
      apn: 'badge'

    },
    sound: {
      apn: 'sound'

    },
    contentAvailable: {
      apn: 'contentAvailable'

    },
    payload: {
      apn: 'payload'
    },
    timeToLive: {
      apn: 'expiry',
      gcm: 'timeToLive'
    }
  },
  text: {
    title: {
      apn: 'alert',
      gcm: 'data.title',
      wns: 'payload.text1'
    },
    body: {
      gcm: 'data.message',
      wns: 'payload.text2'
    }
  }
};

// taken from http://stackoverflow.com/questions/9554479/set-a-propertys-value-with-unknown-depth-within-an-object-in-javascript
// and changed, so that read-operations will not change the object
function updatePath(obj, path, value) {
    var parts = path.split('.');
    var i, tmp;
    for(i = 0; i < parts.length; i++) {
        tmp = obj[parts[i]];
        if(value !== undefined && i == parts.length - 1) {
            tmp = obj[parts[i]] = value;
        }
        else if(tmp === undefined) {
            if(value) {
              tmp = obj[parts[i]] = {};
            } else {
              return undefined;
            }
        }
        obj = tmp;
    }
    return obj;
}

function map(notification, body, service, mapping) {
  Object.keys(mapping).forEach(function(bodyKey) {
    var notificationKey = (mapping[bodyKey] || {})[service],
        bodyValue = updatePath(body, bodyKey);
    if(notificationKey && bodyValue) {
      debug('mapping %j from body.%s to %sNotification.%s', bodyValue, bodyKey, service, notificationKey);
      updatePath(notification, notificationKey, bodyValue);
    }
  });
}

Pushnotifications.prototype.parseBody = function(service, body) {
  var notification = {},
      type = body.type || 'text';

  map(notification, body, service, fieldMappings[type] || {});
  map(notification, body, service, fieldMappings.$common);

  if(body.overrides && body.overrides[service]) {
    _.extend(notification, body.overrides[service]);
  }

  if(service === 'wns') {
    if(type == 'text') {
      notification.type = 'toast';
    }
    if(notification.payload) {
      notification.payload = JSON.stringify(notification.payload);
    }
  }

  return notification;
};
/**
 * Module export
 */
module.exports = Pushnotifications;