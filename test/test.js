if(false) {
    var nock = require('nock');
    try {
        nock.recorder.rec({
            logging: function(content) {
                console.log('-->', content);
            }
        });
    } catch(ex) {
        console.error(ex);
    }
    nock.disableNetConnect();

    after(function(done) {
        this.timeout(10000);
        setTimeout(function() {
            var nockCalls = nock.recorder.play();
            console.log(nockCalls);
            done();
        }, 2500);
    });
}
require('deployd/test/support.js');
expect = require('chai').expect;
var sh = require('shelljs'),
    cb = function(err, res) {
        if(err) throw new Error(err);
        return res;
    },
    db = require('deployd/lib/db'),
    configPath = './test/support/proj/resources/push';

var TestTarget = require('../index');
describe('dpd-push-notify', function() {
    before(function() {
        // reset
        sh.rm('-rf', __dirname + '/support/proj');
        if(!sh.test('-d', __dirname + '/support/proj')) {
            sh.mkdir('-p', __dirname + '/support/proj/resources/push');
            '{}'.to(__dirname + '/support/proj/resources/push/config.json');
        }
    });

    after(function() {
        // reset
        sh.rm('-rf', __dirname + '/support/proj');
    });

    describe('static tests', function() {
        var testTarget, testBodyText;
        before(function() {
            testTarget = new TestTarget('push', { configPath: configPath });
        });
        beforeEach(function() {
            testBodyText = {
                type: 'text',
                title: 'TestTitle',
                body: 'TestBody',
                badge: 123,
                sound: 'TestSound',
                contentAvailable: true,
                payload: {
                    somePayloadKey: 'somePayloadData'
                }
            };
        });

        it('should parse a gcm text-notification', function() {
            expect(testTarget.parseBody('gcm', testBodyText)).to.eql({
                data: {
                    title: testBodyText.title,
                    message: testBodyText.body
                }
            });
        });

        it('should parse an apn text-notification', function() {
            expect(testTarget.parseBody('apn', testBodyText)).to.eql({
                alert: testBodyText.title,
                badge: testBodyText.badge,
                sound: testBodyText.sound,
                contentAvailable: testBodyText.contentAvailable,
                payload: {
                    somePayloadKey: testBodyText.payload.somePayloadKey
                }
            });
        });

        it('should parse an apn text-notification with overrides', function() {
            testBodyText.overrides = {
                apn: {
                    someAPNKey: 'someAPNKey Value',
                    someDeepAPNObject: {
                        someDeepAPNObjectKey: 'someDeepAPNObjectKeyValue'
                    }
                }
            };

            expect(testTarget.parseBody('apn', testBodyText)).to.eql({
                alert: testBodyText.title,
                badge: testBodyText.badge,
                sound: testBodyText.sound,
                contentAvailable: testBodyText.contentAvailable,
                payload: {
                    somePayloadKey: testBodyText.payload.somePayloadKey
                },
                someAPNKey: testBodyText.overrides.apn.someAPNKey,
                someDeepAPNObject: {
                    someDeepAPNObjectKey: testBodyText.overrides.apn.someDeepAPNObject.someDeepAPNObjectKey
                }
            });
            expect(JSON.stringify(testTarget.parseBody('gcm', testBodyText))).not.to.include('someAPNKey');
            expect(JSON.stringify(testTarget.parseBody('wns', testBodyText))).not.to.include('someAPNKey');
        });
    });

    describe('dynamic tests', function() {
        var testTarget;
        before(function() {
            testTarget = new TestTarget('push', {db: db.create(TEST_DB), configPath: configPath, config: {
                apnKey: '/Volumes/simpleTechs/projects/66seconds/backend/resources/push/66seconds.dev.key.pem',
                apnCert: '/Volumes/simpleTechs/projects/66seconds/backend/resources/push/66seconds.dev.cert.pem'
            }});
        });
        beforeEach(function(done){
          testTarget.store.remove(function () {
            testTarget.store.find(function (err, result) {
              expect(err).to.not.exist();
              expect(result).to.eql([]);
              done(err);
            });
          });
        });

        var registerSampleDevice = function(callback) {
            return testTarget.handleRegister({
                userId: 'tralala',
                token: '839c938ab9785cb2c7e7fd6ce0c0b19c700d80b91841d1f9963b156209a995b0',
                deviceType: 'ios'
            }, {
                isRoot: true
            }, function(err, res) {
                expect(err).to.not.exist();
                return callback.apply(this, arguments);
            });
        };

        it('should be instanciated', function() {
            expect(testTarget).to.exist();
            expect(testTarget instanceof TestTarget).to.be.true();
            expect(testTarget).to.have.property('store');
            expect(testTarget).to.respondTo('handleRegister');
        });

        it('should handle registration errors', function() {
            expect(testTarget.handleRegister.bind(testTarget, {}, {}, cb)).to.throw('user is not logged in');
            expect(testTarget.handleRegister.bind(testTarget, {}, {
                isRoot: true
            }, cb)).to.throw('userId and token and deviceType are required');
        });
        it('should handle registration', function(done) {
            testTarget.handleRegister({
                userId: 'tralala',
                token: 'test123',
                deviceType: 'ios'
            }, {
                isRoot: true
            }, function(err, res) {
                expect(err).to.not.exist();
                testTarget.store.find(function (err, result) {
                    expect(err).to.not.exist();
                    expect(result).to.have.length(1);
                    done(err);
                });
            });
        });

        it('should handle dbl registration with no error and no db-update', function(done) {
            registerSampleDevice(function(err, res) {
                testTarget.store.find(function (err, result) {
                    expect(err).to.not.exist();
                    expect(result).to.have.length(1);
                    registerSampleDevice(function(err, res) {
                        testTarget.store.find(function (err, result) {
                            expect(err).to.not.exist();
                            expect(result).to.have.length(1);
                            done(err);
                        });
                    });
                });
            });
        });

        // it('should send an ios notification', function(done) {
        //     registerSampleDevice(function(err, res) {
        //         testTarget.handleSend({}, {}, function(err, res) {
        //             done(err);
        //         });
        //     });
        // });
    });
});